import React from 'react';

class MountComponent extends React.Component {
    constructor(props) {
        super(props);
        this.state = { color: "red" };
    }

    componentDidMount() {
        setTimeout(() => {
            this.setState({color: "blue"})
        }, 4000)
    }

    render() {
        return (
            <h1>My favourite color is {this.state.color} </h1>
        )
    }
}

export default MountComponent