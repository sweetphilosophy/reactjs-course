import React from "react";

class RandomColorComponent extends React.Component {
  constructor(props) {
    super(props);
    this.state = { color: "white" };
  }

  handleRandomColorChange = () => {
    let color = Math.floor(Math.random() * 16777215).toString(16);

    console.log(color);
    this.setState({ color: color });
  };

  render() {
    return (
      <div>
        <h1>
          My random color is:{" "}
          <div style={{ color: "#" + this.state.color }}>#########</div>{" "}
        </h1>
        <button onClick={this.handleRandomColorChange}>Change random color</button>
      </div>
    );
  }
}

export default RandomColorComponent;
