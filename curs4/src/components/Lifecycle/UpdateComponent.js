import React from 'react';

class UpdateComponent extends React.Component {
    constructor(props) {
        super(props);
        this.state = { color: "red" };
    }

    handleColorChange = () => {
        this.setState({color: "green"});
    }

    render() {
        return (
            <div>
                <h1>My favourite color is {this.state.color} </h1>
                <button onClick={this.handleColorChange}>Change color</button>
            </div>
        )
    }
}

export default UpdateComponent