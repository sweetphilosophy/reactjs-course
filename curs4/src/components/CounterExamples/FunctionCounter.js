import React, {useState} from "react";

function CounterFunction() {
    const [count, setCount] = useState(0);

    const incrementItem = () => {
        setCount(count + 1);
    }

    const decreaseItem = () => {
        setCount(count - 1);
    }

    return (
        <div>
            <button onClick={incrementItem}>Click to increment</button>
            <button onClick={decreaseItem}>Click to decrease</button>
            <button onClick={() => setCount(0)}>Reset</button>
            <h1>Count is {count}</h1>
        </div>
    )
}

export default CounterFunction