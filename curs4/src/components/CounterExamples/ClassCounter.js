import React from "react"

class ClassCounter extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            value: 0,
            show: true
        };
    }

    incrementItem = () => {
        this.setState({ value: this.state.value + 1 });
    }

    decreaseItem = () => {
        this.setState({ value: this.state.value - 1 });
    }

    toggleVisibiliyItem = () => {
        this.setState({ show: !this.state.show });
    }

    render() {
        return (
            <div>
                {/* <h1>My counter {this.state.value}</h1> */}
                {this.state.show ? <h1>{this.state.value}</h1> : ""}
                <button onClick={this.incrementItem}>Click to increment</button>
                <button onClick={this.decreaseItem}>Click to decrease</button>
                {/* <button onClick={this.toggleVisibiliyItem}>Change visibility</button> */}
                <button onClick={this.toggleVisibiliyItem}>
                    { this.state.show ? "Hide number" : "Show number" }
                </button>
            </div>
        )
    }

}

export default ClassCounter