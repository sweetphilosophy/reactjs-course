import "./App.css";
import MountComponent from "./components/Lifecycle/MountComponent";
import UpdateComponent from "./components/Lifecycle/UpdateComponent";
import ClassCounter from "./components/CounterExamples/ClassCounter";
import CounterFunction from "./components/CounterExamples/FunctionCounter";
import RandomColorComponent from "./components/Lifecycle/RandomColorComponent";

function App() {
  return (
    <div className="App">
      {/* <MountComponent /> */}
      {/* <UpdateComponent /> */}
      <RandomColorComponent />
      {/* <ClassCounter /> */}
      <CounterFunction />
    </div>
  );
}

export default App;
