import React from "react";
import ImageGalleryComponent from "../image-gallery-component/imageGalleryComponent";
import ProfileInfoComponent from "../profile-info-component/profileInfoComponent";

function MyProfileComponent() {
  return (
    <div>
      <br />
      <br />
      <ProfileInfoComponent />
      <br />
      <br />
      <hr />
      <br />
      <ImageGalleryComponent />
    </div>
  );
}

export default MyProfileComponent;
