import React from "react";
import "./storiesComponent.css"
import Avatar from '@mui/material/Avatar';
import Stack from '@mui/material/Stack';
import Divider from '@mui/material/Divider';

function StoriesComponent() {
    return (
        <Stack direction="row" spacing={2} divider={<Divider orientation="vertical" flexItem />}>
            <Avatar sx={{ width: 56, height: 56 }}>A</Avatar>
            <Avatar sx={{ width: 56, height: 56 }}>B</Avatar>
            <Avatar sx={{ width: 56, height: 56 }}>C</Avatar>
            <Avatar sx={{ width: 56, height: 56 }}>D</Avatar>
            <Avatar sx={{ width: 56, height: 56 }}>E</Avatar>
            <Avatar sx={{ width: 56, height: 56 }}>F</Avatar>
            <Avatar sx={{ width: 56, height: 56 }}>G</Avatar>
        </Stack>
    );
}

export default StoriesComponent;