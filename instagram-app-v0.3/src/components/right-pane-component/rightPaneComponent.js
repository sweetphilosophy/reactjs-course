import React, { useEffect, useState } from "react";
import "./rightPaneComponent.css";
import Avatar from "@mui/material/Avatar";
import ProfilePicture from "../../static/images/Avatar1.png";
import Grid from "@mui/material/Grid";
import userService from "../../services/user-service/userService";
import RightPaneFollowHeadComponent from "../right-pane-follow-head-component/rightPaneFollowHeadComponent";

function RightPaneComponent(props) {
  const [suggestions, setSuggestions] = useState();

  useEffect(() => {
    async function fetchMyApi() {
      let suggestionsFromApi =
        await userService.getRandomFollowSuggestionsForUser(props.user.id, 4);

      setSuggestions(suggestionsFromApi);
    }

    fetchMyApi();
  }, []);

  return (
    <div>
      <Grid container spacing={2}>
        <Grid item xs={4}>
          <Avatar
            alt="Avatar"
            src={ProfilePicture}
            sx={{ width: 75, height: 75 }}
          />
        </Grid>
        <Grid item xs={6}>
          <p>Profile Username</p>
          <p>{props.user.username}</p>
        </Grid>
        <Grid item xs={2}>
          <div className="textColor">
            <p>Switch</p>
          </div>
        </Grid>
      </Grid>

      <Grid container spacing={2}>
        <Grid item xs={8}>
          <p>Suggestions for you</p>
        </Grid>
        <Grid item xs={4}>
          <p>See all</p>
        </Grid>
      </Grid>

      {suggestions &&
        suggestions.length > 0 &&
        suggestions.map((suggestedUser) => (
          <RightPaneFollowHeadComponent
            followHead={suggestedUser}
            loggedUser={props.user}
          />
        ))}

      <div className="footer">
        <p>About</p>
        <p>Help</p>
        <p>Press</p>
        <p>API</p>
        <p>Jobs</p>
        <p>Privacy</p>
        <p>Terms</p>
        <p>Locations</p>
      </div>

      <div className="footer">
        <p>Top Accounts</p>
        <p>Hashtags</p>
        <p>Language</p>
      </div>

      <div className="footer">
        <p>© 2021 INSTAGRAM FROM META</p>
      </div>
    </div>
  );
}

export default RightPaneComponent;
