import Grid from "@mui/material/Grid";
import FooterComponent from "./components/footer-component/footerComponent";
import HeaderComponent from "./components/header-component/headerComponent";
import ImageGalleryComponent from "./components/image-gallery-component/imageGalleryComponent";
import MainFeedComponent from "./components/main-feed-component/mainFeedComponent";
import ProfileInfoComponent from "./components/profile-info-component/profileInfoComponent";
import { BrowserRouter, Routes, Route } from "react-router-dom";
import MyProfileComponent from "./components/my-profile-component/myProfileComponent";
import { useEffect, useState } from "react";
import Login from "./components/login-component/loginComponent";
import Register from "./components/register-component/registerComponent";

function App() {
  const [user, setUser] = useState(undefined);

  useEffect(() => {
    let storageUser = localStorage.getItem("user");
    if (storageUser) {
      setUser(JSON.parse(storageUser));
    }
  }, []);

  if (!user) {
    return (
      <BrowserRouter>
        <Routes>
          <Route path="/" element={<Login setUser={setUser} />} />
          <Route path="register" element={<Register />} />
        </Routes>
      </BrowserRouter>
    );
  } else {
    return (
      <div>
        <Grid container spacing={3}>
          <Grid item xs={3}></Grid>
          <Grid item xs={6}>
            <BrowserRouter>
              <HeaderComponent setUser={setUser} />
              <Routes>
                <Route path="/" element={<MainFeedComponent user={user}/>} />
                <Route path="my-profile" element={<MyProfileComponent />} />
              </Routes>
            </BrowserRouter>
          </Grid>
          <Grid item xs={3}></Grid>
        </Grid>
      </div>
    );
  }
}

export default App;
