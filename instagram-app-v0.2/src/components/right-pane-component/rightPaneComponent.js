import React from "react";
import "./rightPaneComponent.css"
import Avatar from '@mui/material/Avatar';
import ProfilePicture from "../../static/images/Avatar1.png";
import Grid from '@mui/material/Grid';

function RightPaneComponent() {
    return (
        <div>

            <Grid container spacing={2}>
                <Grid item xs={4}>
                    <Avatar
                        alt="Avatar"
                        src={ProfilePicture}
                        sx={{ width: 75, height: 75 }}
                    />
                </Grid>
                <Grid item xs={6}>
                    <p>Profile Name</p>
                    <p>John Smith</p>
                </Grid>
                <Grid item xs={2}>
                    <div className="textColor">
                        <p>Switch</p>
                    </div>
                </Grid>
            </Grid>

            <Grid container spacing={2}>
                <Grid item xs={8}>
                    <p>Suggestions for you</p>
                </Grid>
                <Grid item xs={4}>
                    <p>See all</p>
                </Grid>
            </Grid>


            <Grid container spacing={2}>
                <Grid item xs={2}>
                    <Avatar sx={{ width: 40, height: 40 }}>A</Avatar>
                </Grid>
                <Grid item xs={6}>
                    <div className="fontSize">
                        <p>Alex</p>
                    </div>
                </Grid>
                <Grid item xs={2}>
                    <div className="textColor">
                        <p>Follow</p>
                    </div>
                </Grid>
            </Grid>

            <Grid container spacing={2}>
                <Grid item xs={2}>
                    <Avatar sx={{ width: 40, height: 40 }}>B</Avatar>
                </Grid>
                <Grid item xs={6}>
                    <div className="fontSize">
                        <p>Bogdan</p>
                    </div>
                </Grid>
                <Grid item xs={2}>
                    <div className="textColor">
                        <p>Follow</p>
                    </div>
                </Grid>
            </Grid>

            <Grid container spacing={2}>
                <Grid item xs={2}>
                    <Avatar sx={{ width: 40, height: 40 }}>C</Avatar>
                </Grid>
                <Grid item xs={6}>
                    <div className="fontSize">
                        <p>Cezar</p>
                    </div>
                </Grid>
                <Grid item xs={2}>
                    <div className="textColor">
                        <p>Follow</p>
                    </div>
                </Grid>
            </Grid>

            <Grid container spacing={2}>
                <Grid item xs={2}>
                    <Avatar sx={{ width: 40, height: 40 }}>D</Avatar>
                </Grid>
                <Grid item xs={6}>
                    <div className="fontSize">
                        <p>Damian</p>
                    </div>
                </Grid>
                <Grid item xs={2}>
                    <div className="textColor">
                        <p>Follow</p>
                    </div>
                </Grid>
            </Grid>

            <div className="footer">
                <p>About</p>
                <p>Help</p>
                <p>Press</p>
                <p>API</p>
                <p>Jobs</p>
                <p>Privacy</p>
                <p>Terms</p>
                <p>Locations</p>
            </div>

            <div className="footer">
                <p>Top Accounts</p>
                <p>Hashtags</p>
                <p>Language</p>
            </div>

            <div className="footer">
                <p>© 2021 INSTAGRAM FROM META</p>
            </div>

        </div >

    );
}

export default RightPaneComponent;