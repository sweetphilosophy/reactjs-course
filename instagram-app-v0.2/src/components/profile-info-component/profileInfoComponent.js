import React from "react";
import Grid from '@mui/material/Grid';
import Avatar from '@mui/material/Avatar';
import ProfilePicture from "../../static/images/Avatar1.png"
import Typography from '@mui/material/Typography';
import Button from '@mui/material/Button';
import "./profileInfoComponent.css"
import SettingsIcon from '@mui/icons-material/Settings';

function ProfileInfoComponent() {
    return (
        <div>
            <Grid container spacing={2}>
                <Grid item xs={4}>
                    <div className="profilePictureWrapper">
                        <Avatar
                            alt="Avatar"
                            src={ProfilePicture}
                            sx={{ width: 150, height: 150 }}
                        />
                    </div>
                </Grid>

                <Grid item xs={8}>
                    <div className="profileSection">
                        <Typography variant="h6" component="div" gutterBottom>
                            Profile name
                        </Typography>
                        <Button variant="outlined">Edit profile</Button>
                        <SettingsIcon />
                    </div>
                    <br />
                    <div className="profileSection">
                        <Typography variant="h8" component="div" gutterBottom>
                            <b>98</b> posts
                        </Typography>
                        <Typography variant="h8" component="div" gutterBottom>
                            <b>923</b> followers
                        </Typography>
                        <Typography variant="h8" component="div" gutterBottom>
                            <b>421</b> following
                        </Typography>
                    </div>
                </Grid>
            </Grid>
        </div>
    );
}

export default ProfileInfoComponent;