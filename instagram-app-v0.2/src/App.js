import Grid from '@mui/material/Grid';
import FooterComponent from './components/footer-component/footerComponent';
import HeaderComponent from './components/header-component/headerComponent';
import ImageGalleryComponent from './components/image-gallery-component/imageGalleryComponent';
import MainFeedComponent from './components/main-feed-component/mainFeedComponent';
import ProfileInfoComponent from './components/profile-info-component/profileInfoComponent';

function App() {
  return (
    <div>
      <Grid container spacing={3}>
        <Grid item xs={3}>
        </Grid>
        <Grid item xs={6}>
          <HeaderComponent />
          <MainFeedComponent />
          {/* <br />
          <br />
          <ProfileInfoComponent />
          <br />
          <br />
          <hr />
          <br />
          <ImageGalleryComponent />
          <br />
          <FooterComponent /> */}
        </Grid>
        <Grid item xs={3}>
        </Grid>
      </Grid>
    </div>
  );
}

export default App;
