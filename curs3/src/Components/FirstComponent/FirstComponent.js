import React from "react";

class FirstComponent extends React.Component {

    render(){
        const initialText = "First component with const";

        return(
            <div>
                <p>{initialText}</p>
                <h1>First component</h1>   
            </div>
        )
    }
}

export default FirstComponent