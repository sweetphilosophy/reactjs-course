import React from "react"

class Greetings extends React.Component {
    render(){
        return(
            <div>
                <h1>Welcome {this.props.name}</h1>
            </div>
        )
    }
}

export default Greetings
