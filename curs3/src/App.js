import './App.css';
import FirstComponent from './Components/FirstComponent/FirstComponent';
import Greetings from './Components/PropsComponent/PropsComponent';


function App() {
  const name = "Cezar";

  return (
    <div className="center">
      Welcome react 
      <FirstComponent />
      <Greetings name={name} />
    </div>
  );
}

export default App;
