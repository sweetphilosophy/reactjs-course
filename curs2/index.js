import fetch from "node-fetch";
// const fetch = require("node-fetch");

const URL = "https://catfact.ninja/fact";

async function main() {
	console.log("1");

	// fetch(URL).then(function (response) {
	// 	// console.log(response);
	// 	response.json().then(function (factJson) {
	// 		console.log("primul fact: " + factJson);

	//         // daca vrei promise-urile secventiale trebuie sa bagi urmatorul promise aici.
	// 	});
	// });

	// fetch(URL).then(function (response) {
	// 	// console.log(response);
	// 	response.json().then(function (factJson) {
	// 		console.log("al doilea fact: " + factJson);
	// 	});
	// });

	try {
		let httpResponse = await fetch(URL);
		let fact = await httpResponse.json();

		console.log(fact);
	} catch (error) {
		console.log(error);
	} finally {
		// ceva ce se executa indiferent de succes sau eroare
	}

	console.log("2");
}

main();
console.log("Madalin");
