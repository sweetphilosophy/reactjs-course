import axios from "axios";

const API_URL = "https://tranquil-basin-44316.herokuapp.com/";

class UserService {
  async getRandomFollowSuggestionsForUser(userId, countOfSuggestions = 4) {
    let response = await axios.get(
      API_URL + "random-follow-suggestions/" + userId + "/" + countOfSuggestions
    );

    return response.data;
  }

  async follow(submitterUserId, followsUserId) {
    let response = await axios.post(
      API_URL + "follow/" + submitterUserId + "/" + followsUserId
    );
    return response.data;
  }

  async saveUserPreference(userId, darkTheme) {
    let userPreference = await axios.post(
      API_URL + "user-preferences/" + userId,
      {
        darkTheme,
      }
    );
  }

  async getFeedForUser(userId) {
    let response = await axios.get(API_URL + "feed/" + userId);
    return response.data;
  }
}

export default new UserService();
