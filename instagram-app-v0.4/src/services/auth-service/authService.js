import axios from "axios";

const API_URL = "https://tranquil-basin-44316.herokuapp.com/";

class AuthService {
  async login(username, password) {
    let response = await axios.post(API_URL + "login", { username, password });
    return response.data;
  }

  async register(username, email, password) {
    return await axios.post(API_URL + "register", {
      username,
      email,
      password,
    });
  }
}

export default new AuthService();
