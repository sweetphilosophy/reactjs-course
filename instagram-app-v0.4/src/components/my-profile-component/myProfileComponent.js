import React from "react";
import ImageGalleryComponent from "../image-gallery-component/ImageGalleryComponent";
import ProfileInfoComponent from "../profile-info-component/profileInfoComponent";
import axios from "axios";

const API_URL = "https://tranquil-basin-44316.herokuapp.com/";
function MyProfileComponent(props) {
  const { user } = props;

  const getUserImages = async function () {
    // GET image gallery from database
    try {
      const getURL = `${API_URL}images-for-user/${user.id}`;
      await axios.get(getURL).then((res) => {
        // PUSH images into imageArray / picturesArray
        props.setImageArray(res.data);
        console.log("res.data", res.data);
      });
    } catch (error) {
      console.log(error);
    }
  };
  // Why useEffect ?
  // We use useEffect(function, []) to mimic componentDidMount behavior.
  // this means that the function that we send as first param will be executed only once when the component is rendered for the first time
  React.useEffect(() => {
    // props.setImageArray(picturesArray);
    getUserImages();
    console.log(user.id);
  }, []);

  return (
    <div>
      <br />
      <br />
      <ProfileInfoComponent />
      <br />
      <br />
      <hr />
      <br />
      <ImageGalleryComponent imageArray={props.imageArray} />
    </div>
  );
}

export default MyProfileComponent;
