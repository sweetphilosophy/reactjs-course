import React from 'react';
import './Modal.css';
import { Dialog, DialogContent, DialogTitle } from '@mui/material';
import CloseIcon from '@mui/icons-material/Close';
import IconButton from '@mui/material/IconButton';
import KeyboardBackspaceRoundedIcon from '@mui/icons-material/KeyboardBackspaceRounded';
import Button from '@mui/material/Button';

export default function Modal(props) {
  const { title, openModal, setOpenModal, closeModal, image, uploadImage } =
    props;
  const isModalOpen = openModal;

  return (
    <form>
      {isModalOpen ? (
        <IconButton size="large" className="close-button" onClick={closeModal}>
          <CloseIcon sx={{ width: 35, height: 35, color: 'white' }} />
        </IconButton>
      ) : null}
      <Dialog
        open={openModal}
        onClose={closeModal}
        classes={{ paper: 'modal-container' }}
        BackdropProps={{ style: { backgroundColor: 'rgba(0,0,0,.85)' } }}>
        <DialogTitle className="modal-title">
          <IconButton className={image.imgData ? '' : 'hidden'}>
            <KeyboardBackspaceRoundedIcon
              sx={{ width: 35, height: 35, color: 'black' }}
            />
          </IconButton>
          <div className="dialog-title">{title}</div>
          <Button
            className={image.imgData ? '' : 'hidden'}
            sx={{ width: 35, height: 35, color: 'black' }}
            onClick={uploadImage}>
            Upload
          </Button>
        </DialogTitle>
        <DialogContent dividers className="dialog-content">
          {props.children}
        </DialogContent>
      </Dialog>
    </form>
  );
}
