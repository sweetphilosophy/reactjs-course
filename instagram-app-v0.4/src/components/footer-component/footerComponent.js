import React			from 'react';
import List				from '@mui/material/List';
import ListItem			from '@mui/material/ListItem';
import ListItemText		from '@mui/material/ListItemText';
import MenuItem			from '@mui/material/MenuItem';
import Menu				from '@mui/material/Menu';
import Typography		from '@mui/material/Typography';

import "./footerComponent.css";

export default function FooterComponent() {
	const [anchorEl, setAnchorEl] = React.useState(null);
	const [selectedIndex, setSelectedIndex] = React.useState(1);
	const open = Boolean(anchorEl);
	const handleClickListItem = (event) => {
		setAnchorEl(event.currentTarget);
	};

	const handleMenuItemClick = (event, index) => {
		setSelectedIndex(index);
		setAnchorEl(null);
	};

	const handleClose = () => {
		setAnchorEl(null);
	};

	return (
		<div>
			<div className="footerLinks">
				<a href="/#">Company</a>
				<a href="/#">About</a>
				<a href="/#">Blog</a>
				<a href="/#">Jobs</a>
				<a href="/#">Help</a>
				<a href="/#">Privacy</a>
				<a href="/#">Terms</a>
				<a href="/#">Top Accounts</a>
				<a href="/#">Hashtags</a>
				<a href="/#">Locations</a>
				<a href="/#">App Lite</a>
				<a href="/#">Suggested Accounts</a>
			</div>
			<div className="footerLinks">
				<List
					component="nav"
					aria-label="Language Sellect"
					sx={{ bgcolor: 'background.paper' }}
				>
					<ListItem
						button
						id="language-button"
						aria-haspopup="listbox"
						aria-controls="language-menu"
						aria-label="Language"
						aria-expanded={open ? 'true' : undefined}
						onClick={handleClickListItem}
					>
						<ListItemText
							primary={options[selectedIndex]}
						/>
					</ListItem>
				</List>
				<Menu
					id="language-menu"
					anchorEl={anchorEl}
					open={open}
					onClose={handleClose}
					MenuListProps={{
						'aria-labelledby': 'language-button',
						role: 'listbox',
					}}
				>
					{options.map((option, index) => (
						<MenuItem
							key={option}
							disabled={index === 0}
							selected={index === selectedIndex}
							onClick={(event) => handleMenuItemClick(event, index)}
						>
							{option}
						</MenuItem>
					))}
				</Menu>

				<Typography className="copyright" variant="div" component="div" gutterBottom>
					<b>&copy; Copyright</b> Instahram from Company
				</Typography>
			</div>
		</div>
	);
}

const options = [
	'Languages',
	'English',
	'Abkhazian',
	'Afar',
	'Afrikaans',
	'Akan',
	'Albanian',	
	'Amharic',
	'Arabic',
	'Aragonese',
	'Armenian',
	'Assamese',
	'Avaric',
	'Avestan',
	'Aymara',
	'Azerbaijani',
	'Bambara',
	'Bashkir',
	'Basque',
	'Belarusian',
	'Bengali',
	'Bihari languages',
	'Bislama',
	'Bosnian',
	'Breton',
	'Bulgarian',
	'Burmese',
	'Catalan, Valencian',
	'Central Khmer',
	'Chamorro',
	'Chechen',
	'Chichewa, Chewa, Nyanja',
	'Chinese',
	'Chuvash',
	'Cornish',
	'Corsican',
	'Cree',
	'Croatian',
	'Czech',
	'Danish',
	'Divehi, Dhivehi, Maldivian',
	'Dutch, Flemish',
	'Dzongkha',
	'Esperanto',
	'Estonian',
	'Ewe',
	'Faroese',
	'Fijian',
	'Finnish',
	'French',
	'Fulah',
	'Gaelic, Scottish Gaelic',
	'Galician',
	'Ganda',
	'Georgian',
	'German',
	'Gikuyu, Kikuyu',
	'Greek (Modern)',
	'Greenlandic, Kalaallisut',
	'Guarani',
	'Gujarati',
	'Haitian, Haitian Creole',
	'Hausa',
	'Hebrew',
	'Herero',
	'Hindi',
	'Hiri Motu',
	'Hungarian',
	'Icelandic',
	'Ido',
	'Igbo',
	'Indonesian',
	'Inuktitut',
	'Inupiaq',
	'Irish',
	'Italian',
	'Japanese',
	'Javanese',
	'Kannada',
	'Kanuri',
	'Kashmiri',
	'Kazakh',
	'Kinyarwanda',
	'Komi',
	'Kongo',
	'Korean',
	'Kwanyama, Kuanyama',
	'Kurdish',
	'Kyrgyz',
	'Lao',
	'Latin',
	'Latvian',
	'Letzeburgesch, Luxembourgish',
	'Limburgish, Limburgan, Limburger',
	'Lingala',
	'Lithuanian',
	'Luba-Katanga',
	'Macedonian',
	'Malagasy',
	'Malay',
	'Malayalam',
	'Maltese',
	'Manx',
	'Maori',
	'Marathi',
	'Marshallese',
	'Mongolian',
	'Nauru',
	'Navajo, Navaho',
	'Northern Ndebele',
	'Ndonga',
	'Nepali',
	'Northern Sami',
	'Norwegian',
	'Norwegian Bokmål',
	'Norwegian Nynorsk',
	'Nuosu, Sichuan Yi',
	'Occitan (post 1500)',
	'Ojibwa',
	'Oriya',
	'Oromo',
	'Ossetian, Ossetic',
	'Pali',
	'Panjabi, Punjabi',
	'Pashto, Pushto',
	'Persian',
	'Polish',
	'Portuguese',
	'Quechua',
	'Romanian',
	'Russian',
	'Samoan',
	'Sango',
	'Sanskrit',
	'Sardinian',
	'Serbian',
	'Shona',
	'Sindhi',
	'Sinhala, Sinhalese',
	'Slovak',
	'Slovenian',
	'Somali',
	'Sotho, Southern',
	'South Ndebele',
	'Spanish, Castilian',
	'Sundanese',
	'Swahili',
	'Swati',
	'Swedish',
	'Tagalog',
	'Tahitian',
	'Tajik',
	'Tamil',
	'Tatar',
	'Telugu',
	'Thai',
	'Tibetan',
	'Tigrinya',
	'Tonga',
	'Tsonga',
	'Tswana',
	'Turkish',
	'Turkmen',
	'Twi',
	'Uighur, Uyghur',
	'Ukrainian',
	'Urdu',
	'Uzbek',
	'Venda',
	'Vietnamese',
	'Volap_k',
	'Walloon',
	'Welsh',
	'Western Frisian',
	'Wolof',
	'Xhosa',
	'Yiddish',
	'Yoruba',
	'Zhuang, Chuang',
	'Zulu'
];