import { Grid } from "@mui/material";
import ImageGalleryComponent from "../image-gallery-component/ImageGalleryComponent";
import axios from "axios";
import userService from "../../services/user-service/userService";
import { useEffect, useState } from "react";

export default function MainFeedContentComponent(props) {
  const { user } = props;

  const [feedImagesArray, setFeedImagesArray] = useState([]);

  useEffect(async () => {
    let resultArrayImages = await userService.getFeedForUser(user.id);
    setFeedImagesArray(resultArrayImages);
  }, []);

  return (
    <Grid sx={12}>
      <ImageGalleryComponent
        columns={1}
        imageArray={feedImagesArray}
      ></ImageGalleryComponent>
    </Grid>
  );
}
