import React, { useEffect, useState } from "react";
import "./darkModeComponent.css";
import Brightness4Icon from "@mui/icons-material/Brightness4";
import userService from "../../services/user-service/userService";

function DarkMode(props) {
  const [theme, setTheme] = useState("light");

  useEffect(() => {
    let theme = props.user?.preferences?.darkTheme || "light";
    setTheme(theme);
  }, []);

  let clickedClass = "clicked";
  const body = document.body;
  const lightTheme = "light";
  const darkTheme = "dark";

  if (theme === lightTheme || theme === darkTheme) {
    body.classList.add(theme);
  } else {
    body.classList.add(lightTheme);
  }

  const persistTheme = async (theme) => {
    await userService.saveUserPreference(props.user.id, theme);
  };

  const switchTheme = (e) => {
    if (theme === darkTheme) {
      body.classList.replace(darkTheme, lightTheme);
      e.target.classList.remove(clickedClass);
      localStorage.setItem("theme", "light");
      setTheme(lightTheme);
    } else {
      body.classList.replace(lightTheme, darkTheme);
      e.target.classList.add(clickedClass);
      localStorage.setItem("theme", "dark");
      setTheme(darkTheme);
    }
    persistTheme(theme === "dark" ? "light" : "dark");
  };

  return (
    <button
      className={theme === "dark" ? clickedClass : ""}
      id="darkMode"
      onClick={(e) => switchTheme(e)}
    >
      <div>
        <Brightness4Icon />
      </div>
    </button>
  );
}

export default DarkMode;
