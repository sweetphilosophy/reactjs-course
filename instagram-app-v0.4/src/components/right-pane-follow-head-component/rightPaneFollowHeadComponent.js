import React from "react";
import Avatar from "@mui/material/Avatar";
import ProfilePicture from "../../static/images/Avatar1.png";
import Grid from "@mui/material/Grid";
import { Button } from "@mui/material";
import userService from "../../services/user-service/userService";

export default function RightPaneFollowHeadComponent(props) {
  const follow = async () => {
    await userService.follow(props.loggedUser.id, props.followHead.id);
  };

  return (
    <Grid container spacing={2}>
      <Grid item xs={2}>
        <Avatar sx={{ width: 40, height: 40 }}>A</Avatar>
      </Grid>
      <Grid item xs={6}>
        <div className="fontSize">
          <p>{props.followHead.username}</p>
        </div>
      </Grid>
      <Grid item xs={2}>
        <div className="textColor">
          <Button onClick={follow}>Follow</Button>
          {/* stuff stuff cu users id */}
        </div>
      </Grid>
    </Grid>
  );
}
