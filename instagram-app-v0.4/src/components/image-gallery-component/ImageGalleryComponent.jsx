import React from "react";
import ImageList from "@mui/material/ImageList";
import ImageListItem from "@mui/material/ImageListItem";
import "./ImageGalleryComponent.css";
import ImageListItemBar from "@mui/material/ImageListItemBar";

export default function ImageGalleryComponent(props) {
  const { imageArray, column } = props;
  const columns = props.columns || 3; //=> se evalueaza ca or, adica daca columns = 2, o sa ia valoarea 2
  //                                                          , adica daca columns = 0, o sa ia valoarea 3

  // const column = props ?? 3; // se evalueaza astfel: daca columns = 2, o sa ia valoarea 2
  //                                                    daca column = 0 , o sa ia valoarea 0

  return (
    <div className="imageGallery">
      <ImageList sx={{ width: "100%", height: "100%" }} cols={columns} gap={20}>
        {imageArray.map((item) => (
          <ImageListItem key={item.url} style={{ border: "2px solid white" }}>
            <img
              // src={`${item.url}?w=150&h=150&fit=crop&auto=format`}
              // srcSet={`${item.url}?w=150&h=150&fit=crop&auto=format&dpr=2 2x`}
              // alt={item.title}
              src={item.image ? item.image : item}
              loading="lazy"
            />
            {item.title && item.author && (
              <ImageListItemBar title={item.title} subtitle={item.author} />
            )}
          </ImageListItem>
        ))}
      </ImageList>
    </div>
  );
}
