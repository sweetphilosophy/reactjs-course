import * as React from "react";
import { styled, alpha } from "@mui/material/styles";
import AppBar from "@mui/material/AppBar";
import Box from "@mui/material/Box";
import Toolbar from "@mui/material/Toolbar";
import IconButton from "@mui/material/IconButton";
import Typography from "@mui/material/Typography";
import InputBase from "@mui/material/InputBase";
import SearchIcon from "@mui/icons-material/Search";
import HomeIcon from "@mui/icons-material/Home";
import TelegramIcon from "@mui/icons-material/Telegram";
import AddBoxOutlinedIcon from "@mui/icons-material/AddBoxOutlined";
import ExploreOutlinedIcon from "@mui/icons-material/ExploreOutlined";
import FavoriteBorderOutlinedIcon from "@mui/icons-material/FavoriteBorderOutlined";
import AccountCircleOutlinedIcon from "@mui/icons-material/AccountCircleOutlined";
import LogoutIcon from "@mui/icons-material/Logout";
import { Link, useLocation, useNavigate } from "react-router-dom";

const Search = styled("div")(({ theme }) => ({
  position: "relative",
  borderRadius: theme.shape.borderRadius,
  backgroundColor: alpha(theme.palette.common.white, 0.15),
  "&:hover": {
    backgroundColor: alpha(theme.palette.common.white, 0.25),
  },
  marginRight: theme.spacing(2),
  marginLeft: 0,
  width: "100%",
  [theme.breakpoints.up("sm")]: {
    marginLeft: theme.spacing(3),
    width: "auto",
  },
}));

const SearchIconWrapper = styled("div")(({ theme }) => ({
  padding: theme.spacing(0, 2),
  height: "100%",
  position: "absolute",
  pointerEvents: "none",
  display: "flex",
  alignItems: "center",
  justifyContent: "center",
}));

const StyledInputBase = styled(InputBase)(({ theme }) => ({
  color: "inherit",
  "& .MuiInputBase-input": {
    padding: theme.spacing(1, 1, 1, 0),
    // vertical padding + font size from searchIcon
    paddingLeft: `calc(1em + ${theme.spacing(4)})`,
    transition: theme.transitions.create("width"),
    width: "100%",
    [theme.breakpoints.up("md")]: {
      width: "20ch",
    },
  },
}));

export default function HeaderComponent(props) {
  const navigate = useNavigate();
  const logout = () => {
    props.setUser(undefined);
    localStorage.removeItem("user");
    navigate("/");
  };
  let location = useLocation();

  return (
    <Box sx={{ flexGrow: 1 }}>
      <AppBar position="static">
        <Toolbar style={{ backgroundColor: "white" }}>
          <Typography
            variant="h6"
            noWrap
            component="div"
            color="black"
            sx={{ display: { xs: "none", sm: "block" } }}
          >
            Instagram
          </Typography>
          <Search style={{ color: "black" }}>
            <SearchIconWrapper>
              <SearchIcon />
            </SearchIconWrapper>
            <StyledInputBase
              placeholder="Search…"
              inputProps={{ "aria-label": "search" }}
            />
          </Search>
          <Box sx={{ flexGrow: 1 }} />
          <Box sx={{ display: { xs: "none", md: "flex" } }}>
            <Link to="/">
              <IconButton size="large" aria-label="" color="secondary">
                <HomeIcon />
              </IconButton>
            </Link>

            <IconButton size="large" aria-label="" color="secondary">
              <TelegramIcon />
            </IconButton>

            <Link
              to="/upload-picture"
              state={{
                from: location.pathname,
                backgroundLocation: location.pathname,
              }}
            >
              <IconButton
                size="large"
                aria-label=""
                color="inherit"
                // onClick={handleOpen}
              >
                <AddBoxOutlinedIcon sx={{ width: 25, height: 25 }} />
              </IconButton>
            </Link>

            <IconButton size="large" aria-label="" color="secondary">
              <ExploreOutlinedIcon />
            </IconButton>

            <IconButton size="large" aria-label="" color="secondary">
              <FavoriteBorderOutlinedIcon />
            </IconButton>

            <Link to="/my-profile">
              <IconButton size="large" aria-label="" color="secondary">
                <AccountCircleOutlinedIcon />
              </IconButton>
            </Link>
            <IconButton
              size="large"
              aria-label=""
              color="secondary"
              onClick={logout}
            >
              <LogoutIcon />
            </IconButton>
          </Box>
        </Toolbar>
      </AppBar>
    </Box>
  );
}
