import React from "react";
import Grid from "@mui/material/Grid";
import "./mainFeedComponent.css";
import StoriesComponent from "../stories-component/storiesComponent";
import RightPaneComponent from "../right-pane-component/rightPaneComponent";
import MainFeedContentComponent from "../main-feed-content-component/mainFeedContentComponent";

function MainFeedComponent(props) {
  return (
    <div>
      <Grid container spacing={2}>
        <Grid item xs={8}>
          <br />
          <br />
          <StoriesComponent />
          <br />
          <MainFeedContentComponent user={props.user} />
        </Grid>

        <Grid item xs={4}>
          <br />
          <br />
          <RightPaneComponent user={props.user} />
        </Grid>
      </Grid>
    </div>
  );
}

export default MainFeedComponent;
