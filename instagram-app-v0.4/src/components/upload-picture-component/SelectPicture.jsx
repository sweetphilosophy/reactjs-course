import React from 'react';
import './SelectPicture.css';
import AddToPhotosIcon from '@mui/icons-material/AddToPhotos';
import Button from '@mui/material/Button';
import { styled } from '@mui/material/styles';

const Input = styled('input')({
  display: 'none',
});

export default function SelectPicture(props) {
  const { setImage } = props;
  const [picture, setPicture] = React.useState(null);
  const onChangePicture = e => {
    if (e.target.files[0]) {
      // console.log('picture: ', e.target.files[0]);
      setPicture(e.target.files[0]);
      const reader = new FileReader();
      reader.addEventListener('load', () => {
        setImage({
          name: '',
          date: new Date(),
          imgData: reader.result,
          file: e.target.files[0],
        });
      });
      reader.readAsDataURL(e.target.files[0]);
    }
  };
  return (
    <div className="upload-form">
      <AddToPhotosIcon sx={{ width: 90, height: 90, color: 'black' }} />
      <h2>Drag photos and videos here</h2>
      <label htmlFor="contained-button-file">
        <Input
          accept="image/*"
          id="contained-button-file"
          type="file"
          onChange={onChangePicture}
        />
        <Button variant="contained" component="span">
          Select from computer
        </Button>
      </label>
    </div>
  );
}
