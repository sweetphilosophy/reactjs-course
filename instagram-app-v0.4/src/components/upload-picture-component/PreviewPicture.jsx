import React from 'react';
import './PreviewPicture.css';

export default function PreviewPicture(props) {
  const { imgData } = props;
  return (
    <div className="preview-container">
      <img className="preview-picture" src={imgData} />
    </div>
  );
}
