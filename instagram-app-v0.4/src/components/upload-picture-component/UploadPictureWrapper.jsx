import React from 'react';
import { useLocation, useNavigate } from 'react-router-dom';
import Modal from '../modal-component/Modal';
import SelectPicture from './SelectPicture';
import PreviewPicture from './PreviewPicture';
import axios from 'axios';
const API_URL = 'https://tranquil-basin-44316.herokuapp.com/';

export default function UploadPictureWrapper(props) {
  const { user, imageArray } = props;
  const [openModal, setOpenModal] = React.useState(true);
  const [image, setImage] = React.useState({
    name: '',
    date: '',
    imgData: '',
  });
  const navigate = useNavigate();
  const location = useLocation();
  console.log(location.state);
  let title = 'Create new post';

  const handleOpen = () => setOpenModal(true);
  const handleClose = () => {
    setOpenModal(false);
    navigate(`${location.state?.from ?? '/'}`);
  };

  const uploadImage = async function () {
    console.log(image.imgData);
    // POST formData
    try {
      const postURL = `${API_URL}upload-picture/${user.id}`;
      await axios.post(postURL, { image: image.imgData }).then(res => {
        console.log(res);

        // Close Modal
        handleClose();
      });
    } catch (ex) {
      console.log(ex);
    }
  };
  // console.log(image?.date);

  // console.log(user.id);
  // console.log(imageArray);

  return (
    <Modal
      openModal={openModal}
      setOpenModal={setOpenModal}
      title={title}
      closeModal={handleClose}
      image={image}
      uploadImage={uploadImage}
      //
    >
      {image.imgData ? (
        <PreviewPicture imgData={image.imgData} />
      ) : (
        <SelectPicture setImage={setImage} />
      )}
    </Modal>
  );
}
