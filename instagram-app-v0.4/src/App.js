import Grid from "@mui/material/Grid";
import HeaderComponent from "./components/header-component/headerComponent";
import MainFeedComponent from "./components/main-feed-component/mainFeedComponent";
import { BrowserRouter, Routes, Route } from "react-router-dom";
import MyProfileComponent from "./components/my-profile-component/myProfileComponent";
import { useEffect, useState } from "react";
import Login from "./components/login-component/loginComponent";
import Register from "./components/register-component/registerComponent";
import DarkMode from "./components/dark-mode-component/darkModeComponent";
import UploadPictureWrapper from "./components/upload-picture-component/UploadPictureWrapper"

function App() {
  const [user, setUser] = useState(undefined);
  const [imageArray, setImageArray] = useState([]);

  useEffect(() => {
    let storageUser = localStorage.getItem("user");
    if (storageUser) {
      setUser(JSON.parse(storageUser));
    }
  }, []);

  if (!user) {
    return (
      <BrowserRouter>
        <Routes>
          <Route path="/" element={<Login setUser={setUser} />} />
          <Route path="register" element={<Register />} />
        </Routes>
      </BrowserRouter>
    );
  } else {
    return (
      <div>
        <Grid container spacing={3}>
          <Grid item xs={3}>
            <DarkMode user={user} />
          </Grid>
          <Grid item xs={6}>
            <BrowserRouter>
              <HeaderComponent setUser={setUser} />
              <Routes>
                <Route path="/" element={<MainFeedComponent user={user} />} />
                <Route
                  path="my-profile"
                  element={
                    <MyProfileComponent
                      imageArray={imageArray}
                      setImageArray={setImageArray}
                      user={user}
                    />
                  }
                />
                <Route
                  path="/upload-picture"
                  element={
                    <UploadPictureWrapper
                      imageArray={imageArray}
                      setImageArray={setImageArray}
                      user={user}
                    />
                  }
                />
              </Routes>
            </BrowserRouter>
          </Grid>
          <Grid item xs={3}></Grid>
        </Grid>
      </div>
    );
  }
}

export default App;
