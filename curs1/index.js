// console.log("Hello world");

// let x = 10;

// console.log(x);

// function add(a, b) {
// 	return a + b;
// }

// let diff = (a, b) => a - b; // echivalent cu { return a - b; }

// console.log(add(5, 10));

// console.log(diff(123, 81));

// let array1 = [
// 	"a",
// 	2,
// 	true,
// 	{ nume: "Mircea", prenume: "Iordache" },
//     [1,2,3],
// 	function () {
// 		console.log("hello");
// 	},
// ];

// console.log(array1);

// console.log(array1[3].nume);

// array1.sort(  );

// console.log(array1);

function greeting(helloMessage, name) {
	console.log(helloMessage() + name);
}

function sayHelloEnglish() {
	return "hello "; // get text value from intl file
}

function sayHelloRomanian() {
	return "buna ziua "; // get text value from intl file
}

function destroy() {
	return "destroy ";
}

// greeting(destroy, "Stefan");

let myArray = [
	{ nume: "Anghelus", prenume: "Crina" },
	{ nume: "Iordache", prenume: "Mircea" },
	{ nume: "Popa", prenume: "Teodora" },
	{ nume: "Filip", prenume: "Cezar" },
	{ nume: "Oprea", prenume: "Mihai" },
];

// console.log(JSON.stringify(myArray));

myArray.sort((a, b) => a.prenume.localeCompare(b.prenume)).reverse();

// console.log(JSON.stringify(myArray));

console.log(JSON.stringify(myArray));

let filteredArray = myArray.filter((person) => person.prenume[0] === "M");

console.log(JSON.stringify(myArray));
console.log(JSON.stringify(myArray.filter((person) => person.prenume[0] === "M")));
