import Grid from '@mui/material/Grid';
import ImageGalleryComponent from './components/image-gallery-component/imageGalleryComponent';
import ProfileInfoComponent from './components/profile-info-component/profileInfoComponent';

function App() {
  return (
    <div>
      <Grid container spacing={3}>
        <Grid item xs={3}>
        </Grid>
        <Grid item xs={6}>
          <br />
          <br />
          <ProfileInfoComponent />
          <br />
          <br />
          <hr />
          <br />
          <ImageGalleryComponent />
        </Grid>
        <Grid item xs={3}>
        </Grid>
      </Grid>
    </div>
  );
}

export default App;
